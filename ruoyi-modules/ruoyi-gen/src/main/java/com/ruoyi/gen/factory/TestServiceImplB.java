package com.ruoyi.gen.factory;


public class TestServiceImplB implements TestService {

    @Override
    public String firstStep() {
        return "TestServiceImplB--firstStep";
    }

    @Override
    public String secondStep() {
        return "TestServiceImplB--secondStep";
    }

    @Override
    public String thirdStep() {
        return "TestServiceImplB--thirdStep";
    }
}
