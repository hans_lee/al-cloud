package com.ruoyi.gen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.gen.domain.GenTemplate;
import com.ruoyi.gen.domain.entity.GenTemplateEntity;

import java.util.List;

/**
 * 代码生成模板管理1Mapper接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface GenTemplateMapper extends BaseMapper<GenTemplate> {

    /**
     * 查询代码生成模板管理1列表
     *
     * @param genTemplate 代码生成模板管理1
     * @return 代码生成模板管理1集合
     */
    List<GenTemplateEntity> selectGenTemplateList(GenTemplateEntity genTemplate);


}
